# ImarisXT_Histocytometry

These are matlab extensions for the image analysis platform Imaris. 

They are made available as is, with no guarantee of functionality. 

They draw inspiration from multiple sources, including:

EASYXT (https://github.com/BIOP/EasyXT)
Imaris reader (https://github.com/PeterBeemiller/ImarisReader)
Chrysalis (https://github.com/Histo-cytometry/Chrysalis)
IceImarisConnector (https://github.com/aarpon/IceImarisConnector)

To run follow directions here: https://imaris.oxinst.com/learning/view/article/enabling-imagej-fiji-and-matlab-plugins-in-imaris