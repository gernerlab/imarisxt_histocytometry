%
%    <CustomTools>
%      <Menu>
%        <Submenu name="Caleb's Extensions"> 
%          <Item name="Multiple Equation Channel Arithmetics" icon="Matlab" tooltip="Create a new channel combining the others using a regular expression.">
%            <Command>MatlabXT::Calebs_Multi_EQ_ChannelArithmetics_V3(%i)</Command>
%          </Item>
%        </Submenu>
%      </Menu>
%    </CustomTools>
%  Description:
%   
%   Apply user defined expressions to Imaris file creating a new channel for each function. 
% 

function Calebs_Multi_EQ_ChannelArithmetics_V3(aImarisApplicationID)
vPD = waitbar(0, 'Initialize Imaris communication');
%% Initialize Imaris communication
 % get the application object
if isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper')
  % called from workspace
  vImarisApplication = aImarisApplicationID;
else
  % connect to Imaris interface
  javaaddpath ImarisLib.jar
  vImarisLib = ImarisLib;
  if ischar(aImarisApplicationID)
    aImarisApplicationID = round(str2double(aImarisApplicationID));
  end
  vImarisApplication = vImarisLib.GetApplication(aImarisApplicationID);
end

%% Get the current file path

% To find this comand I opened the ImarisLib.jar with 7ZIP
fnm = vImarisApplication.GetCurrentFileName();
fnm = fnm.toCharArray';
fnm = [fnm(1:(end-4)) '_ChArith.ims']
svfl = 1;
% % % pause

%% Define the expression
waitbar(0.2, vPD, 'Waiting on you...');
choice='Yes';
neqts=0;
while choice=='Yes'
    neqts=neqts+1; 
    
    prompt = {'Channel Name', ...
              sprintf(['Enter new channel equation:\n\n', ...
             'Use channel names: ch1, ch2 etc...\nUse matlab operators, i.e. ', ...
             '+, -, .*, ./, .^, sqrt, max, min...           ']), ...
             'Save Name', ...
             'If you do not want Channel arithmatics to save after running, set this to 0'};
    dlg_title = 'Input Channel Arithmatics';
    num_lines = 1;
    defaultans = {'Lymphocytes, B220+CD3','ch8+ch10', fnm, num2str(svfl)};
    vAnswer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    
    % end the program if they hit cancel
    if isempty(vAnswer), return, end
    
    eval(sprintf('vName%i = {vAnswer{1}};', neqts))
    eval(sprintf('vAnswer%i = {vAnswer{2}};', neqts))
    
    fnm = vAnswer{3};
    svfl = str2num(vAnswer{4});

    % Construct a question diolog with two options
    choice = questdlg('Are there more equations you want to apply?', ...
    'Select Multiple Folders', 'Yes','No ', 'No ');    
end

%% Read in the size of the data and number of channels
waitbar(0.3, vPD, 'Reading in some data');
aSizeX = vImarisApplication.GetDataSet.GetSizeX;
aSizeY = vImarisApplication.GetDataSet.GetSizeY;
aSizeT = vImarisApplication.GetDataSet.GetSizeT;
aSizeZ = vImarisApplication.GetDataSet.GetSizeZ;

%% Evaluate the functions
vAnswer = '';
vName = '';
for veqt = 1:neqts
    waitbar(0.3, vPD, 'Get the current number of channels');
    vLastC = vImarisApplication.GetDataSet.GetSizeC;
    eval(sprintf('vAnswer = vAnswer%i', veqt));
    eval(sprintf('vName = vName%i', veqt));
    
    waitbar(0.3, vPD, 'Creating the new channel (why does this take forever?)');
    % Create a new channel directly in Imaris
    vImarisApplication.GetDataSet.SetSizeC(vLastC + 1);
    % Name the channel with the equation used
    vImarisApplication.GetDataSet.SetChannelName(vLastC, vName{1});
    
    waitbar(0.3, vPD, 'Evaluate The Function');
    %Evaluate the equation for each t slice
    for vTime = 1:aSizeT
        %Evaluate the equation for each z slice
      for vSlice = 1:aSizeZ
        for vChannel = 1:vLastC
            waitbar((vChannel)/(vLastC), vPD, ['Copying slice ' num2str(vSlice) ', channel ' num2str(vChannel) ' of ' num2str(vLastC) ' to MATLAB']);
            if contains(vAnswer{1}, sprintf('ch%i', vChannel))
                % works on double to allow division and prevent overflows
                chdat = double(vImarisApplication.GetDataSet.GetDataSubVolumeAs1DArrayFloats(0, 0, vSlice-1, vChannel-1, vTime-1, aSizeX, aSizeY, 1));
                % This sets the data from each channel = to the channel number          
                eval(sprintf('ch%i = chdat;', vChannel));
            end
        end
        waitbar((vTime*vSlice)/(aSizeT*aSizeZ), vPD, 'Calculating');
        clear chdat
        % Evaluates the given arithmatic/function
        try
          vData = eval([vAnswer{1} ';']);
        catch er
          msgbox(sprintf(['Error while evaluating the expression.\n\n', ...
            'Possible causes: invalid variable names (cH 1 instead of ch1 etc...), ', ...
            'invalid operators (use .* instead of *)...\n\n', er.message]));
          return;
        end  
        % Copys the new channel back to Imaris
        waitbar((vTime*vSlice)/(aSizeT*aSizeZ), vPD, ['Copying slice ' num2str(vTime) ', ' num2str(vSlice) ...
            ' of (' num2str(aSizeT) ', ' num2str(aSizeZ) ') back to Imaris']);
        try
          if strcmp(vImarisApplication.GetDataSet.GetType,'eTypeUInt8')
            vImarisApplication.GetDataSet.SetDataSubVolumeAs1DArrayBytes(uint8(vData), 0,0,vSlice-1,vLastC,vTime-1,aSizeX,aSizeY,1);
          elseif strcmp(vImarisApplication.GetDataSet.GetType,'eTypeUInt16')
            vImarisApplication.GetDataSet.SetDataSubVolumeAs1DArrayShorts(uint16(vData), 0,0,vSlice-1,vLastC,vTime-1,aSizeX,aSizeY,1);
          elseif strcmp(vImarisApplication.GetDataSet.GetType,'eTypeFloat')
            vImarisApplication.GetDataSet.SetDataSubVolumeAs1DArrayFloats(single(vData), 0,0,vSlice-1,vLastC,vTime-1,aSizeX,aSizeY,1);
          end
        catch er
          msgbox(sprintf(['The result of the expression is not a valid dataset.\n\n', ...
            'Possible causes: invalid result size.\n\n', er.message]));
          return
        end
      end % end slice loop
    end % end Time loop
    
    if svfl == 1
        waitbar(0.9, vPD, 'Saving File');
        vImarisApplication.FileSave(fnm, '');
    end
    
    waitbar(1, vPD, 'Done!');
end
close(vPD);

      
