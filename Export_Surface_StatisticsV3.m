%    <CustomTools> 
%      <Menu> 
%        <Submenu name="Caleb's Extensions"> 
%          <Item name="Export Surface Statistics to .csv" icon="Matlab"> 
%            <Command>MatlabXT::Export_Surface_StatisticsV3(%i)</Command> 
%          </Item> 
%        </Submenu>
%      </Menu> 
%    </CustomTools> 


%%% Dr. Caleb Stoltzfus, 04-19-2019

function Export_Surface_StatisticsV3(aImarisApplicationID)
    %% Working variables
    vPD = waitbar(0, 'Initialize Imaris communication');
    if isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper')
        vImarisApplication = aImarisApplicationID;
    else
        % Connect to Imaris interface
        if exist('ImarisLib','class') == 0
            javaaddpath ImarisLib.jar
        end
        vImarisLib = ImarisLib;
        if ischar(aImarisApplicationID)
            aImarisApplicationID = round(str2double(aImarisApplicationID));
        end
        vImarisApplication = vImarisLib.GetApplication(aImarisApplicationID);
    end

    % Load Surpass Scene
    aSurpassScene = vImarisApplication.GetSurpassScene;
    % Find the number of channels
    vLastC = vImarisApplication.GetDataSet.GetSizeC;
    vNames = cell(vLastC,2);
    for vChannel = 1:vLastC
         vNames{vChannel,1} = char(vImarisApplication.GetDataSet.GetChannelName(vChannel-1));
         vNames{vChannel,2} = num2str(vChannel);
    end
    IND = find(strcmp(vNames(:,1), '(name not specified)'));
    if contains('(name not specified)', vNames(:,1))
        vNames{strcmp(vNames(:,1), '(name not specified)'),1} = ['Channel_' num2str(IND)];
    end

    % Get the number of objects in the surpass scene
    nb_objects = aSurpassScene.GetNumberOfChildren;
    % pre-allocate an array of surface objects
    surfaces = zeros(nb_objects,1);
    % pre-allocate an array of spots objects
    spots = zeros(nb_objects,1);

    waitbar(0.3, vPD, 'Getting the Surpass Scene');

    
    % Find the surpas scene index of any spots or surfaces
    ObjNames = cell(nb_objects,4);
    Objects = struct;
    for i=0:nb_objects-1
        ObjNames{i+1,2} = 0;
        Objects.(['OBJ' num2str(i)]) = aSurpassScene.GetChild(i);
        if(vImarisApplication.GetFactory.IsSurfaces(Objects.(['OBJ' num2str(i)])))
            surfaces(i+1) = 1;
            ObjNames{i+1,1} = char(Objects.(['OBJ' num2str(i)]).GetName);
            ObjNames{i+1,2} = i+1;
            ObjNames{i+1,3} = 'Surface';
        elseif(vImarisApplication.GetFactory.IsSpots(Objects.(['OBJ' num2str(i)])))
            spots(i+1) = 1;
            ObjNames{i+1,1} = char(Objects.(['OBJ' num2str(i)]).GetName);
            ObjNames{i+1,2} = i+1;
            ObjNames{i+1,3} = 'Spot';
        end
    end
    ObjNames = ObjNames([ObjNames{:,2}]~=0,:);


    %% UI: type of object
    
    waitbar(0.4, vPD, 'Waiting on you to select children');
    cnct = 0;

    % Build a UI table
    tData = cell(numel(ObjNames(:,1)), 3);
    tData(:,1) = {true};
    tData(:,2) = ObjNames(:,1);
    tData(:,3) = {1};

    % Build the clustering options menus
    UIfig = uifigure('Name', 'Select Surfaces');
    UIfig.Position = [10 10 420 800];

    % Create the table of options
    t = uitable(UIfig);
    t.Data = tData;
    t.Position = [0 70 420 730];
    t.ColumnName = {'Export Statistics','Object Name', 'Classifier'};
    t.ColumnEditable = [true false true];
    t.ColumnWidth = {100, 200, 85};

    % Create a Density button
    concatBTN = uibutton(UIfig,'state');
    concatBTN.Position = [10, 10, 200, 50];
    concatBTN.Text = 'Concatenate Statistics';
    concatBTN.Value = false;

    % Create a Start button
    btn = uibutton(UIfig,'push', 'ButtonPushedFcn', @(btn,event) func_OK(t,UIfig,concatBTN.Value));
    btn.Position = [10+200+100, 10, 50, 30];
    btn.Text = 'Ok';

    % Create a Cancel button
    btn = uibutton(UIfig,'push', 'ButtonPushedFcn', @(btn,event) func_Cancel(UIfig));
    btn.Position = [10+200+50+100, 10, 50, 30];
    btn.Text = 'Cancel';

    function func_Cancel(UIfig)
        delete(UIfig)
        return
    end
    function func_OK(t,UIfig,concatBTN)
        cnct = concatBTN;
        ObjNames(:,4) = t.Data(:,3);
        ObjNames = ObjNames([t.Data{:,1}]==1,:);
        delete(UIfig)    
    end

    close(vPD)
    waitfor(UIfig)
    
    vPD = waitbar(0, 'Good choice');

    % The number of objects selected
    ObjNames
    nobj = numel(ObjNames(:,1));
    objidx = cell2mat(ObjNames(:,2))-1;
    
    %% Continue if requested objects exist
    if nobj == 0
        return
    end
    %% Populate objects and get stats names
   
    aObjects = javaArray('Imaris.IDataItemPrxHelper', nobj);
    aNames = cell(0);
    StatNames = cell(0);
    stats = struct;
    
    for i=1:nobj
        
        waitbar((0.5+0.2*(i/nobj)), vPD, ['Loading Statistics from Imaris (This takes a while), ' num2str(i) ' of ' num2str(nobj)]); 
        
% % %         aObjects(i) = Objects.(['OBJ' num2str(i)]);
        aObjects(i) = aSurpassScene.GetChild(objidx(i)); %this line is fast
        
        % Get the stats
        stats.(['OBJ_' num2str(i)]) = aObjects(i).GetStatistics; % This line is slow
        
        waitbar((0.5+0.2*(0.1+i/nobj)), vPD, 'Getting Statistics names'); 
        
        % This section is also slow for some reason
        
        % Get the names organized
        tempNames = cell(stats.(['OBJ_' num2str(i)]).mNames);
        StatNames = [StatNames; unique(tempNames)];

        aFactornames = cell(stats.(['OBJ_' num2str(i)]).mFactorNames);
        aFactors = transpose(cell(stats.(['OBJ_' num2str(i)]).mFactors));
        objtype = strcmp(aFactornames,'Category');
        ch = strcmp(aFactornames,'Channel');
        I = ~cellfun('isempty',aFactors(:,ch));
        tempNames(I) = strcat(tempNames(I), {' Ch_'}, aFactors(I,ch));
        filter = strcmp(aFactors(:,objtype),ObjNames{i,3});
        tempNames = tempNames(filter);
        aNames = [aNames; tempNames];
    end
    
    waitbar(0.65, vPD, 'Fixing names'); 
    
    uniqueNames = unique(aNames, 'stable');
    StatNames = unique(StatNames, 'stable');

    namesAndChannels = cell(numel(uniqueNames),4);
    substrindices = zeros(numel(uniqueNames),4);
    getsubstr = @(x,s,e) x(s:e);
    indices = strfind(uniqueNames,' Ch_');
    I = ~cellfun(@isempty,indices);
    J = ~I;
    namesAndChannels(J,1) = uniqueNames(J);
    namesAndChannels(J,2) = {''};
    substrindices(I,1) = 1;
    substrindices(I,2) = transpose([indices{I}] - 1);
    substrindices(I,3) = transpose([indices{I}] + 4);
    substrindices(I,4) = cellfun(@(x) size(x,2),uniqueNames(I));
    substrindices = num2cell(substrindices);
    namesAndChannels(I,1) =  cellfun(getsubstr,uniqueNames(I),substrindices(I,1),substrindices(I,2),'UniformOutput',false);
    namesAndChannels(I,2) =  cellfun(getsubstr,uniqueNames(I),substrindices(I,3),substrindices(I,4),'UniformOutput',false);
    namesAndChannels(I,4) =  uniqueNames(I);

    %% UI: stats selection
%%%%% 
    waitbar(0.7, vPD, 'Building a table'); 

    % Build a UI table
    defaults = {'Position';'Mean';'Volume';'Sphericity'};
    defaults = contains(StatNames, defaults);    
    
    tData = cell(numel(StatNames), 2);
    tData(:,1) = {false};
    tData(defaults,1) = {true};
    tData(:,2) = StatNames;

    % Build the clustering options menus
    UIfig = uifigure('Name', 'Select Statistics');
    UIfig.Position = [10 10 420 800];

    % Create the table of options
    t = uitable(UIfig);
    t.Data = tData;
    t.Position = [0 70 420 730];
    t.ColumnName = {'Export Statistics','Statistic Name'};
    t.ColumnEditable = [true false];
    t.ColumnWidth = {100, 200};

    % Create a Start button
    btn = uibutton(UIfig,'push', 'ButtonPushedFcn', @(btn,event) func2_OK(t,UIfig));
    btn.Position = [10+200+100, 10, 50, 30];
    btn.Text = 'Ok';

    % Create a Cancel button
    btn = uibutton(UIfig,'push', 'ButtonPushedFcn', @(btn,event) func2_Cancel(UIfig));
    btn.Position = [10+200+50+100, 10, 50, 30];
    btn.Text = 'Cancel';

    function func2_Cancel(UIfig)
        delete(UIfig)
        return
    end
    function func2_OK(t,UIfig)
        StatNames = StatNames([t.Data{:,1}]==1);
        delete(UIfig)    
    end

    close(vPD)
    waitfor(UIfig)
    
    
%%%%%    
    vPD = waitbar(0, 'Another Solid Decision');

    if isempty(StatNames)
        return
    end
    
    
    
    
    waitbar(0.75, vPD, 'Selecting Channel Names'); 
    % Pull the selected stats names
% % %     StatNames = StatNames(selection);
    if numel(StatNames)==1
        StatNames = StatNames{1};
    end
    namesAndChannels = namesAndChannels(ismember(namesAndChannels(:,1),StatNames),:);
    [~, chIND] = ismember(namesAndChannels(:,2), vNames(:,2));
    % add in the actual channel names
    namesAndChannels(chIND~=0,3) = vNames(chIND(chIND~=0),1);
    namesAndChannels(chIND==0,4) = namesAndChannels(chIND==0,1);    

    nselection = numel(namesAndChannels(:,1));
    if nselection == 0
        return;
    end
    
    %% Build the channel names
    namesAndChannels(:,1) = strrep(namesAndChannels(:,1), ' ', '_');
% % %     namesAndChannels(:,1) = strrep(namesAndChannels(:,1), 'Position', '');
    namesAndChannels(:,1) = strrep(namesAndChannels(:,1), 'Intensity', 'Int');
    namesAndChannels(:,1) = strcat(namesAndChannels(:,1), '_');
    
    namesAndChannels(:,4) = strrep(namesAndChannels(:,4), ' ', '_');
% % %     namesAndChannels(:,4) = strrep(namesAndChannels(:,4), 'Position', '');
    namesAndChannels(:,4) = strrep(namesAndChannels(:,4), 'Intensity', 'Int');
    
    % Build the actual channel names
    ChNames = namesAndChannels(:,3);
    % Put in the non-channel headers, like position etc.
    INDemty = cellfun(@isempty,ChNames);
    namesAndChannels(INDemty,1) = strrep(namesAndChannels(INDemty,1), '_', '');
    namesAndChannels(INDemty,4) = strrep(namesAndChannels(INDemty,4), '_', '');
    ChNames(INDemty) = namesAndChannels(INDemty,1);
    % Put in channel stat and real name
    ChNames(~INDemty) = strcat(namesAndChannels(~INDemty,1), namesAndChannels(~INDemty,3));
    % If the channel is Intensity mean just put the channel name
    INDmean = strcmp(namesAndChannels(:,1), 'Int_Mean_');
    if sum(INDmean)~=0
        ChNames(INDmean) = namesAndChannels(INDmean,3);
    end
    ChNames = [ChNames; 'ID'];
    ChNames = strrep(ChNames, 'Position', '');
    
    namesAndChannels
    ChNames
    
    %% Get the stats
    waitbar(0.8, vPD, 'Preparing to Index channels and Statistics'); 

    Statistics = struct;
    for i=1:nobj
        
        waitbar(0.8 + 0.1*(i/nobj), vPD, ['Indexing statistics (Why is this slow?), ' num2str(i) ' of ' num2str(nobj)]); 

        % This is only returning
        Parameters = cell(stats.(['OBJ_' num2str(i)]).mNames);
        values = stats.(['OBJ_' num2str(i)]).mValues;
        ids = stats.(['OBJ_' num2str(i)]).mIds;
        
        % Heading for the aFactors data
        aFactornames = cell(stats.(['OBJ_' num2str(i)]).mFactorNames);
        % Tags for each element in the values vector describing its channel, catagory, time etc. 
        aFactors = transpose(cell(stats.(['OBJ_' num2str(i)]).mFactors));
        objtype = strcmp(aFactornames,'Category');
        ch = strcmp(aFactornames,'Channel');
        % Pull the index of all of the channel names
        I = ~cellfun('isempty',aFactors(:,ch));
        
        Header = Parameters;
        Header(I) = strrep(strcat(Header(I), {'_Ch_'}, aFactors(I,ch)), ' ', '_');
% % %         Header = strrep(Header, 'Position', '');
        Header = strrep(Header, ' ', '');
        Header = strrep(Header, 'Intensity', 'Int');
        
        % Pull only things from the actual surface
        filter = strcmp(aFactors(:,objtype),ObjNames{i,3});
        values = values(filter);
        ids = ids(filter);
        Header = Header(filter);
        
        % Find the indeces of the selected stats and pull those elements
        [INDCont, ~] = ismember(Header, namesAndChannels(:,4));
        values = values(INDCont);
        ids = ids(INDCont);
        Header = Header(INDCont);
                
% %         uHeader = unique(Header, 'stable');
        % Pull the selected statistics
        if cnct==1
            Statistics.(['OBJ_' num2str(i)]) = zeros(numel(unique(ids)), numel(namesAndChannels(:,4))+2);
        else
            Statistics.(['OBJ_' num2str(i)]) = zeros(numel(unique(ids)), numel(namesAndChannels(:,4))+1);
        end

        for j=1:numel(namesAndChannels(:,4))
            INDj = ismember(Header, namesAndChannels{j,4});           

            % If you have mixed surfaces and spots deal with that
            if sum(INDj)~=0
                
                % This should alwayse be zero, otherwise something is screwing
                % up the order that your objects are in
                if sum(unique(ids, 'stable')-ids(INDj))~=0
                    dlg = warndlg(['The ' namesAndChannels{j,4} ' statistic cannot be accuratly indexed, I blame Imaris.']);
                    waitfor(dlg)
                end
                
                % Put the data in the table in the correct order
                datSub =  values(INDj);
                % Sort the data based on the object IDs, Position is in a
                % random order, but sorting on ID fixes this
                [~, IND_ID] = sort(ids(INDj));
                Statistics.(['OBJ_' num2str(i)])(:,j) = datSub(IND_ID);
            else
                Statistics.(['OBJ_' num2str(i)])(:,j) = ones(numel(unique(ids)), 1);
            end
        end
        [~, IND_ID] = sort(unique(ids, 'stable'));
        DatIDs = unique(ids, 'stable');
        Statistics.(['OBJ_' num2str(i)])(:,j+1) = DatIDs(IND_ID);
                
        if cnct==1
            Statistics.(['OBJ_' num2str(i)])(:,j+2) = ObjNames{i,4}.*ones(numel(unique(ids)), 1);
        end
    end
    
    %% UI: file name
    waitbar(0.95, vPD, 'Getting ready to save'); 

    % Get the current file name
    fnm = vImarisApplication.GetCurrentFileName();
    fnm = fnm.toCharArray';
    [path, name, ~] = fileparts(fnm);
    cd(path);
    %% Write the data
    waitbar(0.95, vPD, 'Saving...'); 
    for i=1:nobj
        if cnct~=1
            
            [filename, folder] = uiputfile('.csv', 'Save File To:', [ObjNames{i,1} '_' name]);
            cd(folder);
            fnm = [folder, filename];

            datMAT = Statistics.(['OBJ_' num2str(i)]);
            headerformat = '%s\n';
            dataformat = '%f\n';

            fid = fopen(fnm,'w');
            if fid ~= -1
                for k=1:(numel(ChNames)-1)
                    headerformat = ['%s, ' headerformat];
                    dataformat = ['%f, ' dataformat];
                end
                fprintf(fid,headerformat,ChNames{:});
                fclose(fid);
                dlmwrite(fnm,datMAT, '-append')
            end
            
        else
            % If you selected concatenate all of the files
            if i==1
                ChNames = [ChNames; 'Classifier'];
                [filename, folder] = uiputfile('.csv', 'Save File To:', ['AllCells_' name]);
                cd(folder);
                fnm = [folder, filename];
                
                headerformat = '%s\n';
                dataformat = '%f\n';
                for k=1:(numel(ChNames)-1)
                    headerformat = ['%s, ' headerformat];
                    dataformat = ['%f, ' dataformat];
                end
                fid = fopen(fnm,'w');
                if fid == -1
                    return
                end
                fprintf(fid,headerformat,ChNames{:});
                fclose(fid);
            end
            % Write the data
            datMAT = Statistics.(['OBJ_' num2str(i)]);
            dlmwrite(fnm,datMAT, '-append','precision',10)
        end
    end
    waitbar(0.7, vPD, 'Done!');      
    close(vPD)
end





