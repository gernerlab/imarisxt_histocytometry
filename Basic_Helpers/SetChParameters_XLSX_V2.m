%    <CustomTools> 
%      <Menu> 
%        <Submenu name="Caleb's Extensions"> 
%          <Item name="Set the channel parameters from an .xlsx file " icon="Matlab"> 
%            <Command>MatlabXT::SetChParameters_XLSX_V2(%i)</Command> 
%          </Item> 
%        </Submenu>
%      </Menu> 
%    </CustomTools> 


%%% Dr. Caleb Stoltzfus, 11-15-2017

function SetChParameters_XLSX_V2(aImarisApplicationID)
% load matlab imarus library definitions
% get the application object
if isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper')
  % called from workspace
  vImarisApplication = aImarisApplicationID;
else
  % connect to Imaris interface
  javaaddpath ImarisLib.jar
  vImarisLib = ImarisLib;
  if ischar(aImarisApplicationID)
    aImarisApplicationID = round(str2double(aImarisApplicationID));
  end
  vImarisApplication = vImarisLib.GetApplication(aImarisApplicationID);
end
%% Load max/min data from a file

% Set the current path to where the currently opened Imaris file is
fnm = vImarisApplication.GetCurrentFileName();
fnm = fnm.toCharArray';
[path, ~, ~] = fileparts(fnm);
cd(path);
%% ask the user to point to the file
% returns the filename (fnm) and the path name (pnm)
[fnm, pnm]=uigetfile({'*.xlsx'}, 'Select the .xlsx file with the desired channel parameters','Multiselect', 'off');
%set the current directory to the path of the user selected file
cd(pnm);
dat00 = readtable(fnm);

nms = fieldnames(dat00)
if any(contains(nms, 'Min'))
    vMin = dat00.(nms{contains(nms, 'Min')})
    vMax = dat00.(nms{contains(nms, 'Max')})
    vNames = dat00.(nms{contains(nms, 'Name')})
else
    % The .xlsx loaded wrong, hope its in the right format and just has
    % NaNs or extranious columns
    vMin = dat00{:, 3}
    vMax = dat00{:, 4}
    vNames = dat00{:, 2}
end
%Convert from normal color format to Imaris's weird format
% vColors = dat00.Color_Red + dat00.Color_Green.*256 + dat00.Color_Blue.*(256^2) + dat00.Color_Transparency.*(256^3);
%% Set the channel parameters in Imaris
% Get the number of channels
vLastC = vImarisApplication.GetDataSet.GetSizeC;
%loop through all of the channels
for vChannel = 1:vLastC
  % Set the channel ranges
  vImarisApplication.GetDataSet.SetChannelRange(vChannel-1, vMin(vChannel), vMax(vChannel));   
  %Set the channel names
  vImarisApplication.GetDataSet.SetChannelName(vChannel-1, vNames{vChannel});
  %Set the channel colors
%   vDataSet.SetChannelColorRGBA(vChannel-1, vColors(vChannel));
end

