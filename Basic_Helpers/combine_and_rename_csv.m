function combine_and_rename_csv()

    %% Load saved gated data
    vPD = waitbar(0, 'Initializing');
    % Define the filepaths
    clear app.fnm app.pnm datt choice n
    dat = struct;
    app = struct;
    app.fnm = [];
    app.svnm = [];
    app.pnm = [];
    waitbar(0.1, vPD, 'Waiting on you');

    %Input the Folder paths and File paths
    [app.fnm , app.pnm]=uigetfile({'*.csv', 'Select files to combine or rename (*.csv)'}, ...
        'Select csv files that you want to modify','Multiselect', 'on');
    if app.pnm==0
        return
    end
    %Convert character array to cell array
    if ischar(app.fnm)
        app.fnm = {app.fnm};
    end
    if iscell(app.pnm)
        app.pnm = app.pnm{1};
    end
    cd(app.pnm);
    app.svnm = app.fnm(1);

    %% Import the data
    waitbar(0.1, vPD, 'Loading data');
    app.Chnms = cell(size(app.fnm, 2), 1);
    for fnm_i = 1:size(app.fnm, 2)
        dat.(['S' num2str(fnm_i)]) = table;
        %open the file
        dat00=importdata([app.pnm app.fnm{fnm_i}]); 
        %Pull out the data

        dattemp = dat00.data;
        chName = strrep(dat00.colheaders, ' ', '');

        dat.(['S' num2str(fnm_i)])= array2table(dattemp);
        dat.(['S' num2str(fnm_i)]).Properties.VariableNames = chName;
        app.Chnms{fnm_i} = dat.(['S' num2str(fnm_i)]).Properties.VariableNames;
    end % end of file
 
    waitbar(0.25, vPD, 'Checking data format');

    %% Open a new figure for the data table
    close(vPD)
    fig = uifigure;
    fig.Position = [10, 10, 550 800];
    %% Put in a table
    app.Table = cell(numel(app.Chnms{1}),numel(fieldnames(dat))+2);
    app.Table(1:numel(app.Chnms{1}),1)= {true};

    colnms = 'Export';
    frmt = ({[]});
    for col_i = 1:numel(fieldnames(dat))
        colnms = [colnms, strrep(app.fnm(col_i), '.csv', '')];
        frmt = [frmt, {dat.(['S' num2str(col_i)]).Properties.VariableNames}];
        
        app.Table(1:numel(app.Chnms{col_i}),(col_i+1))= app.Chnms{col_i}';
        
        
    end
    frmt = {frmt{:}, []};
    colnms = {colnms{:}, 'Final Channel Names'}';

    app.Table(1:numel(app.Chnms{1}),end)= app.Chnms{1};

    % Create the table of options
    app.t = uitable(fig);

    % This makes drop down menus right in the table
    app.t.ColumnFormat = frmt;
    app.t.Data = app.Table;
    app.t.Position = [0 50 900 750];
    app.t.ColumnName = colnms;
    app.t.ColumnEditable = true;
    app.t.ColumnWidth = {50, 100, 100, 150};
    
    swch = uiswitch(fig);
    swch.Items = {'Combine','Rename'};
    swch.Position = [315 10 50 30];
 
    uibutton(fig, 'push', 'Text', 'Save',...
        'Position', [5 10 50 30],...
        'ButtonPushedFcn', @(~, ~) func_save(app, fig, dat,swch));
    
    uibutton(fig, 'push', 'Text', 'Import Channel Names',...
        'Position', [60 10 150 30],...
        'ButtonPushedFcn', @(~, ~) func_loadChNms(app));

    %%
    function func_loadChNms(app)
    %% Input the Folder paths and File paths
        [fnm , pnm]=uigetfile({'*.xlsx; *.csv', 'Channel Names File'}, ...
            'Select file with channel names','Multiselect', 'off');
        vPDl = waitbar(0.5, 'Loading');
        %open the file and load the channel names
        if strcmp(fnm((end-4):end), '.xlsx')
            dat00 = importdata([pnm fnm]); 
            chnames = {dat00.textdata{2:end, 2}}';
            app.t.Data(1:numel(chnames),end) = chnames;
        elseif strcmp(fnm((end-3):end), '.csv')
            dat00 = importdata([pnm fnm]); 
            chnames = strrep(dat00.colheaders, ' ', '');
            app.t.Data(1:numel(chnames),end) = chnames;
        end
        close(vPDl)
    end


    function func_save(app, fig, dat,swch)
        %% Save the file dat.(['S' num2str(pnm_i)])
        vPDs = waitbar(0, 'Saving');
        err = 1;
        for fnm_j = 1:size(app.fnm, 2)
            try
                ChNames = app.t.Data([app.t.Data{:,1}]==1,end);
                % save the channels int he user defined order
                datMAT = dat.(['S' num2str(fnm_j)])(:, app.t.Data(:,(fnm_j+1)));
                % Save only the selected channels
                datMAT = datMAT(:, [app.t.Data{:,1}]==1);

                datMAT = table2array(datMAT);

                waitbar(0.25, vPDs, 'Writing File');
                if fnm_j==1
                    [file,path, idx] = uiputfile('*.csv','Save file name', [app.fnm{1}(1:(end-4)) '_Renamed.csv']);
                    fnm = [path, file];
                    if idx==1                
                        % the user made a filename
                        cd(path);

                        headerformat = '%s\n';
                        dataformat = '%f\n';

                        fid = fopen(fnm,'w');
                        if fid ~= -1
                            for k=1:(numel(ChNames)-1)
                                headerformat = ['%s, ' headerformat];
                                dataformat = ['%f, ' dataformat];
                            end
                            fprintf(fid,headerformat,ChNames{:});
                            fclose(fid);
                            dlmwrite(fnm,datMAT, '-append','precision',7)
                        end

                        waitbar(0.9, vPDs, 'Done!');
                        err = 0;
                    else
                        % do nothing the user didn't select anything
                        close(vPDs)
                    end
                else
                    switch swch.Value
                        case 'Rename'
                            [file,path, idx] = uiputfile('*.csv','Save file name', [app.fnm{fnm_j}(1:(end-4)) '_Renamed.csv']);
% % %                             file = [app.fnm{fnm_j}(1:(end-4)) '_Renamed.csv']
% % %                             idx=1;
                            fnm = [path, file];
                            if idx==1                
                                % the user made a filename
                                cd(path);

                                headerformat = '%s\n';
                                dataformat = '%f\n';

                                fid = fopen(fnm,'w');
                                if fid ~= -1
                                    for k=1:(numel(ChNames)-1)
                                        headerformat = ['%s, ' headerformat];
                                        dataformat = ['%f, ' dataformat];
                                    end
                                    fprintf(fid,headerformat,ChNames{:});
                                    fclose(fid);
                                    dlmwrite(fnm,datMAT, '-append','precision',7)
                                end

                                waitbar(0.9, vPDs, 'Done!');
                                err = 0;
                            else
                                % do nothing the user didn't select anything
                                close(vPDs)
                            end
                            
                        case 'Combine'
                            % If there is already a header just append the data
                            dlmwrite(fnm,datMAT, '-append','precision',7)
                            waitbar(0.9, vPDs, 'Done!');
                            err = 0;
                    end
                end
            catch 
                msgbox('There was a problem saving.')
            end
            if err ==1
                return
            end
        end
        if err ==0
            close(vPDs)
            close(fig)
        end

    end % end of the save function

end
