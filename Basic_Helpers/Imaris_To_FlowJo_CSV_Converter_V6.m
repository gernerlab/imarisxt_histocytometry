classdef Imaris_To_FlowJo_CSV_Converter_V6 < handle
    properties
        % These are my global variables that I will use in the program
        % This initializes the properties of the "app" object
        FileMenu = struct;
        dat = struct;
        fnm = [];
        svnm = [];
        pnm = [];
        t = [];
        concatBTN
        ClassNum
        SpherBTN
        chnames
        Chnms
        
    end

    
    methods(Static)

        function app = Imaris_To_FlowJo_CSV_Converter_V6

            %% Buid the user interface
            fig = figure('Name', 'Convert Imaris .csv files', 'NumberTitle','off');
            fig.DockControls = 'off';
            fig.MenuBar = 'none';
            fig.Color = 'w';
            fig.InvertHardcopy = 'off';
            fig.Position = [10, 10, 550 800];

            % Create the table of options
            app.t = uitable(fig);

            % This makes drop down menus right in the table
            % % % app.t.ColumnFormat = ({[] [] [] app.app.fnmS [] [] [] []});
            app.t.Position = [0 50 900 750];
            app.t.ColumnName = {'Export', 'Standardize', 'Original Name', 'Export Name', 'Folder Name'};
            app.t.ColumnEditable = [true, true, false, true, false];
            app.t.ColumnWidth = {75, 75, 100, 150, 125};
          
            % Create a Classifier button
            app.concatBTN = uicontrol(fig,'Style', 'togglebutton');
            app.concatBTN.Position = [2*50+170+4*5 10 75 30];
            app.concatBTN.String = 'Add Classifier';
            app.concatBTN.Value = true;
            app.concatBTN.BackgroundColor = [0.9 1 0.9];

            app.ClassNum = uicontrol(fig,'Style', 'edit');
            app.ClassNum.Position = [2*50+170+75+5*5 10 50 30];
            app.ClassNum.String = '1';

            app.SpherBTN = uicontrol(fig,'Style', 'togglebutton');
            app.SpherBTN.Position = [2*50+170+75+50+6*5 10 100 30];
            app.SpherBTN.String = 'Add Sphericity';
            app.SpherBTN.Value = true;
            app.SpherBTN.BackgroundColor = [0.9 1 0.9];

            app.concatBTN.Callback =  @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_Classifier(app);
            app.SpherBTN.Callback =  @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_Classifier(app);

            % Create Menu bar
            app.FileMenu.Main = uimenu(fig);
            app.FileMenu.Main.Text = 'File';

            % Create a load data option
            app.FileMenu.load = uimenu(app.FileMenu.Main);
            app.FileMenu.load.MenuSelectedFcn = @(btn,event) Imaris_To_FlowJo_CSV_Converter_V6.func_load_csv(app, fig, 0);
            app.FileMenu.load.Text = 'Import Single Surface Object';

            % Create a load multiple samples
            app.FileMenu.loadmulti = uimenu(app.FileMenu.Main);
            app.FileMenu.loadmulti.MenuSelectedFcn = @(btn,event) Imaris_To_FlowJo_CSV_Converter_V6.func_load_csv(app, fig, 1);
            app.FileMenu.loadmulti.Text = 'Import Multiple Surface Objects';
            
            % Create Load Names Button
            app.FileMenu.laodbtn = uimenu(app.FileMenu.Main);
            app.FileMenu.laodbtn.MenuSelectedFcn = @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_load_Names(app);
            app.FileMenu.laodbtn.Text = 'Load Channel Names From .csv';

            % Create Save Button
            app.FileMenu.savebtn = uicontrol('Style', 'pushbutton', 'String', 'Save');
            app.FileMenu.savebtn.Position = [5 10 50 30];
            app.FileMenu.savebtn.Callback = @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_save(app, fig);

            % Create append button
            app.FileMenu.appendbtn = uicontrol('Style', 'pushbutton', 'String', 'Append');
            app.FileMenu.appendbtn.Position = [50+2*5 10 50 30];
            app.FileMenu.appendbtn.Callback = @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_append(app, fig);


        end

        function func_load_csv(app, fig, multi)
            %% Load saved gated data
            vPD = waitbar(0, 'Initializing');
            waitbar(0.1, vPD, 'Waiting on you');
            
            app.fnm = [];
            app.pnm = [];
            app.svnm = [];
            app.Chnms = [];
                        
            if multi==0
                %% 'Single Sample'
                    %Input the Folder paths and File paths
                    [app.fnm , app.pnm]=uigetfile({'*.csv', 'Imaris Statistics files (*.csv)'}, ...
                        'Select Imaris statistics csv files that you want to concatenate','Multiselect', 'on');
                    
                    if app.pnm==0
                        close(vPD)
                        return
                    else
                        app.dat = struct;
                    end
                    
                    %Convert character array to cell array
                    if ischar(app.fnm)
                        app.fnm = {app.fnm};
                    end
                    % don't load the Overall csv
                    app.fnm(contains(app.fnm, 'Overall')) = [];
                    cd(app.pnm);

                    app.pnm = {app.pnm};
                    app.fnm = {app.fnm};
                    
                    app.svnm = strsplit(app.pnm{1}, filesep);
                    if isempty(app.svnm{end})
                        app.svnm = app.svnm(end-1);
                    else
                        app.svnm = app.svnm(end);
                    end
% % %                     app.svnm = {app.fnm{1}{1}};
            else
                %% 'Multiple Samples'
                %Input the Folder paths and File paths
                app.pnm = Imaris_To_FlowJo_CSV_Converter_V6.uigetdir2('', ...
                    'Shift + Select folders with .csv files in them');
                % If no file was selected cancel function
                if isempty(app.pnm)
                    close(vPD); 
                    return
                else
                    app.dat = struct;
                end
                
                app.fnm = cell(numel(app.pnm), 1);
                for pnm_i = 1:numel(app.pnm)
                    fnm_i = dir(fullfile(app.pnm{pnm_i}, '*.csv'));
                    % Pull out just the names
                    app.fnm{pnm_i} = strcat('\',{fnm_i.name});
                    
                    app.svnm{pnm_i} = strsplit(app.pnm{pnm_i}, filesep);
                    app.svnm{pnm_i} = app.svnm{pnm_i}{end};
                    
                    % don't load the Overall csv
                    app.fnm{pnm_i}(contains(app.fnm{pnm_i}, 'Overall')) = [];
                    
                    % Access filenames using app.fnm{app.pnm_i}{app.fnm_i} syntax

                        % Sort the file names to not be in a stupid order
                        numbers = str2double(regexp(app.fnm{pnm_i}, '\d+\.', 'match', 'once'));
                        [~, IND] = sort(numbers);
                        app.fnm{pnm_i} = app.fnm{pnm_i}(IND);
                end
            end

            % Import the data
            waitbar(0.1, vPD, 'Loading data');
            for pnm_i = 1:size(app.fnm, 1)
                app.dat.(['S' num2str(pnm_i)]) = table;
                for fnm_i = 1:size(app.fnm{pnm_i}, 2)
                    %open the file
                    dat00=importdata([app.pnm{pnm_i} app.fnm{pnm_i}{fnm_i}]); 
                    %Pull out the data
                    if strcmp(dat00.textdata{1,1}, 'Position') % If position pull x,y, and z
                        dattemp = [{dat00.textdata{4:end,1}}]';
                        dattemp = str2double(dattemp);
                        app.dat.(['S' num2str(pnm_i)]).(strrep(dat00.textdata{3,1}, ' ', ''))= dattemp;

                        dattemp = [{dat00.textdata{4:end,2}}]';
                        dattemp = str2double(dattemp);
                        app.dat.(['S' num2str(pnm_i)]).(strrep(dat00.textdata{3,2}, ' ', ''))= dattemp;

                        dattemp = [{dat00.textdata{4:end,3}}]';
                        dattemp = str2double(dattemp);
                        app.dat.(['S' num2str(pnm_i)]).(strrep(dat00.textdata{3,3}, ' ', ''))= dattemp;

                        % Import the channel ID
                        app.dat.(['S' num2str(pnm_i)]).chID = dat00.data(:,2);


                    else  % if not position just pull the data
                        dattemp = [{dat00.textdata{4:end,1}}]';
                        dattemp = str2double(dattemp);
                        chName = strrep(strrep(strrep(strrep(dat00.textdata{1,1}, ...
                            '=', ''), ...
                            ' ', '_'), ...
                            'Intensity_Mean', 'IntMn'), ...
                            ',', '');
                        app.dat.(['S' num2str(pnm_i)]).(chName)= dattemp;
                    end 
                end % end of channel loop
                % Read the variable names
                if pnm_i~=1
                    log = strcmp(app.Chnms, strrep(app.dat.(['S' num2str(pnm_i)]).Properties.VariableNames, '_Img1', ''));
                    if sum(~log)~=0
                        warndlg(['Warning Channel:  ' app.fnm{pnm_i}{~log} ' Does not exist in all samples'])
                    end
                end
                app.Chnms = app.dat.(['S' num2str(pnm_i)]).Properties.VariableNames;
                app.Chnms = strrep(app.Chnms, '_Img1', '');
            end % end of sample loop
            waitbar(0.25, vPD, 'Checking data format');

            %% Put in a table
            Table = cell(max(numel(app.Chnms), numel(app.svnm)),5);
            Table(1:numel(app.Chnms),1)= {true};
            Table(1:numel(app.Chnms),2)= {false};
            Table(1:numel(app.Chnms),3)= app.Chnms;
            Table(1:numel(app.Chnms),4)= app.Chnms;
            Table(1:numel(app.svnm),5)= app.svnm;
            
            app.t.Data = Table;

% %             app.FileMenu.savebtn.Callback = @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_save(app, fig);
% %             app.FileMenu.appendbtn.Callback = @(~, ~) Imaris_To_FlowJo_CSV_Converter_V6.func_append(app, fig);

            app.FileMenu.load.Text = 'Import New Single Surface Object';
            app.FileMenu.loadmulti.Text = 'Import Multiple New Surface Objects';

            close(vPD)

        end

        function func_Classifier(app)  
            if app.concatBTN.Value
                app.ClassNum.Visible = true;
                app.concatBTN.BackgroundColor = [0.9 1 0.9];
            else
                app.ClassNum.Visible = false;
                app.concatBTN.BackgroundColor = [1 0.9 0.9];
            end
            if app.SpherBTN.Value
                app.SpherBTN.BackgroundColor = [0.9 1 0.9];
            else
                app.SpherBTN.BackgroundColor = [1 0.9 0.9];
            end
        end

        function func_load_Names(app)
        %% Input the Folder paths and File paths
            [fnm , pnm]=uigetfile({'*.xlsx; *.csv', 'Channel Names File'}, ...
                'Select file with channel names','Multiselect', 'off');
            if fnm==0
                return
            end
            %open the file and load the channel names
            if strcmp(fnm((end-4):end), '.xlsx')
                dat00 = importdata([pnm fnm]); 
                app.chnames = {dat00.textdata{2:end, 2}}';
                app.t.Data(1:numel(app.chnames),4) = app.chnames;
            elseif strcmp(fnm((end-3):end), '.csv')
                dat00 = importdata([pnm fnm]); 
                app.chnames = dat00.textdata';
                if size(app.chnames, 2)>1
                    app.chnames = app.chnames(:,1);
                end
                
                indNch = min(numel(app.t.Data(:,4)), numel(app.chnames));
                app.t.Data(1:indNch,4);
                app.t.Data(1:indNch,4) = app.chnames(1:indNch);
            end
        end

        function func_save(app, fig)
            %% Save the file dat.(['S' num2str(pnm_i)])

            vPD = waitbar(0, 'Saving');
            err = 1;
            for pnm_i = 1:size(app.fnm, 1)
                try
                    svnms = app.t.Data(1:numel(app.Chnms),5);
                                      
                    [file, path, idx] = uiputfile('*.csv','Save file name', strcat(svnms{pnm_i}  , '.csv'));
                                               
                    if idx~=1 || sum(path==0)==1 || sum(file==0)==1
                        close(vPD)
                        return
                    end
                    cd(path)
                    
                    % Save only the selected channels
                    indKEEP = [app.t.Data{1:numel(app.Chnms),1}]==1;
                    datMAT = app.dat.(['S' num2str(pnm_i)])(:, indKEEP);
                    
                    ChNames = app.t.Data(1:numel(app.Chnms),4);
                    ChNames = ChNames(indKEEP,:);
                            
                    % add the sphericity channel to the data table
                    if app.SpherBTN.Value
                        if ~ismember('Sphericity',fieldnames(datMAT))
                            datMAT.Sphericity = ones(size(datMAT, 1), 1);
                            if contains('Volume', ChNames)
                                datMAT = ...
                                    movevars(datMAT,'Sphericity','Before','Volume');
                                indV = find(contains(ChNames, 'Volume'));
                                ChNames = [ChNames(1:(indV-1)); 'Sphericity'; ChNames(indV:end)];
                            else
                                ChNames = [ChNames; 'Sphericity'];
                            end
                        end
                    end
                    
                    % Add the classifier value to the data table
                    if app.concatBTN.Value
                        class  = zeros(size(datMAT, 1),1) + str2double(app.ClassNum.String);
                        datMAT.Classifier = class;
                        ChNames = [ChNames; 'Classifier'];
                    end
                    
                    datMAT = table2array(datMAT);
                    
                    % Standardize: Put data into form of (mu=0, sigma=1)
                    indSTD = [app.t.Data{indKEEP,2}]==1;
                    if sum(indSTD)~=0
                        if size(datMAT, 1)~=1

                            datMAT(:, indSTD) = datMAT(:, indSTD) - mean(datMAT(:, indSTD));
                            stdeva = std(datMAT(:, indSTD));
                            stdeva(stdeva==0) = 1; % account for single cells
                            datMAT(:, indSTD) = (datMAT(:, indSTD))./stdeva;
                        else
                            datMAT(:, indSTD) = datMAT(:, indSTD).*0;
                        end
                    end
                   
                    waitbar(0.25, vPD, 'Writing File');
                    fnm = [path, file];
                    if idx==1                
                        % the user made a filename
                        cd(path);

                        headerformat = '%s\n';
                        dataformat = '%f\n';

                        fid = fopen(fnm,'w');
                        if fid ~= -1
                            for k=1:(numel(ChNames)-1)
                                headerformat = ['%s, ' headerformat];
                                dataformat = ['%f, ' dataformat];
                            end
                            fprintf(fid,headerformat,ChNames{:});
                            fclose(fid);
                            dlmwrite(fnm,datMAT, '-append','precision',7)
                        end

                        waitbar(0.9, vPD, 'Done!');
                        err = 0;
                    else
                        % do nothing the user didn't select anything
                        close(vPD)
                    end

                catch 
                    msgbox('There was a problem saving.')
                end

            end
            
            if err ==0
                close(vPD)
            end

        end

        function func_append(app, fig)
            %% Save the file dat.(['S' num2str(pnm_i)])
            vPD = waitbar(0, 'Saving');
            err = 1;
            for pnm_i = 1:size(app.fnm, 1)
                try
                    svnms = app.t.Data(1:numel(app.Chnms),5);

                    % Save only the selected channels
                    indKEEP = [app.t.Data{1:numel(app.Chnms),1}]==1;
                    datMAT = app.dat.(['S' num2str(pnm_i)])(:, indKEEP);
                    
                    ChNames = app.t.Data(1:numel(app.Chnms),4);
                    ChNames = ChNames(indKEEP,:);

                    % add the sphericity channel to the data table
                    if app.SpherBTN.Value
                        if ~ismember('Sphericity',fieldnames(datMAT))
                            datMAT.Sphericity = ones(size(datMAT, 1),1);
                            if contains('Volume', ChNames)
                                datMAT = ...
                                    movevars(datMAT,'Sphericity','Before','Volume'); 
                            end
                        end
                    end
                    
                    % Add the classifier value to the data table
                    if app.concatBTN.Value
                        class  = zeros(size(datMAT, 1),1) + str2double(app.ClassNum.String);
                        datMAT.Classifier = class;
                    end
                    
                    datMAT = table2array(datMAT);

                    % Standardize: Put data into form of (mu=0, sigma=1)
                    indSTD = [app.t.Data{indKEEP,2}]==1;
                    if sum(indSTD)~=0
                        if size(datMAT, 1)~=1
                            datMAT(:, indSTD) = datMAT(:, indSTD) - mean(datMAT(:, indSTD));
                            stdeva = std(datMAT(:, indSTD));
                            stdeva(stdeva==0) = 1; % account for single cells
                            datMAT(:, indSTD) = (datMAT(:, indSTD))./stdeva;
                        else
                            datMAT(:, indSTD) = datMAT(:, indSTD).*0;
                        end
                    end

                    waitbar(0.25, vPD, 'Writing File');

                    [file,path, idx] = uigetfile('*.csv','Save file name', svnms{pnm_i});
                    fnm = [path, file];
                    if idx==1                
                        cd(path);
                        dlmwrite(fnm,datMAT, '-append','precision',7)
                        waitbar(0.9, vPD, 'Done!');
                        err = 0;
                    else
                        % do nothing the user didn't select anything
                        close(vPD)
                    end

                catch 
                    msgbox('There was a problem saving.')
                end

            end
            if err ==0
                close(vPD)
                clear
            end
        end

        % Load multiple directories
        function [pathname] = uigetdir2(start_path, dialog_title)
        % Pick multiple directories and/or files
        import javax.swing.JFileChooser; % I am not sure exactly how this works
        if nargin == 0 || isempty(start_path) || start_path == 0 % Allow a null argument.
            start_path = pwd;
        end
        jchooser = javaObjectEDT('javax.swing.JFileChooser', start_path);
        jchooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        if nargin > 1
            jchooser.setDialogTitle(dialog_title);
        end
        jchooser.setMultiSelectionEnabled(true);
        status = jchooser.showOpenDialog([]);
        if status == JFileChooser.APPROVE_OPTION
            jFile = jchooser.getSelectedFiles();
            pathname{size(jFile, 1)}=[];
            for i=1:size(jFile, 1)
                pathname{i} = char(jFile(i).getAbsolutePath);
            end
        elseif status == JFileChooser.CANCEL_OPTION
            pathname = [];
        else
            error('Error occured while picking file.');
        end
    end
    
    end % end of methods and functions
end % end of class definition