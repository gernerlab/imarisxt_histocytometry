%
%    <CustomTools>
%      <Menu>
%        <Submenu name="Caleb's Extensions"> 
%          <Item name="Batched Multiple Equation Channel Arithmetics" icon="Matlab" tooltip="Create a new channel combining the others using a regular expression.">
%            <Command>MatlabXT::Calebs_Multi_EQ_Batch_ChannelArithmetics_V2(%i)</Command>
%          </Item>
%        </Submenu>
%      </Menu>
%    </CustomTools>
%  Description:
%   
%   Apply a user defined expression to multiple Imaris files creating a new channel in each file. 
% 

function Calebs_Multi_EQ_Batch_ChannelArithmetics_V2(aImarisApplicationID)
vPD = waitbar(0, 'Initialize Imaris communication');
%% Initialize Imaris communication
% get the application object
if isa(aImarisApplicationID, 'Imaris.IApplicationPrxHelper')
  % called from workspace
  vImarisApplication = aImarisApplicationID;
else
  %% connect to Imaris interface
  javaaddpath ImarisLib.jar
  vImarisLib = ImarisLib;
  if ischar(aImarisApplicationID)
    aImarisApplicationID = round(str2double(aImarisApplicationID));
  end
  vImarisApplication = vImarisLib.GetApplication(aImarisApplicationID);
end

%% Define the expression
waitbar(0.2, vPD, 'Waiting on you...');
choice='Yes';
neqts=0;
while choice=='Yes'
    neqts=neqts+1;
    prompt = {sprintf('This function wil close the currently open file without saving! \n\n Channel Name:'), ...
              sprintf(['Enter new channel equation:\n\n', ...
    'Use channel names: ch1, ch2 etc...\nUse matlab operators, i.e. ', ...
    '+, -, .*, ./, .^, sqrt, max, min...           '])};
    dlg_title = 'Input Channel Arithmatics';
    num_lines = 1;
    defaultans = {'Lymphocytes, B220+CD3','ch8+ch10'};
    vAnswer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    % end the program if they hit cancel
    if isempty(vAnswer), return, end
    
    eval(sprintf(['vName%i = {vAnswer{1}}'], neqts));
    eval(sprintf(['vAnswer%i = {vAnswer{2}}'], neqts));

    % Construct a question diolog with two options
    choice = questdlg(sprintf(['Are there more equations you want to apply?']), ...
    'Select Multiple Folders', 'Yes','No ', 'No ');    
end

%% Ask for the file paths of the files you want to change
clear fnm pnm datt
choice = 'Yes';
n=0;
% Keep loading filepaths until the user is happy
while choice=='Yes'
    n=n+1;
    %Input the Folder paths and File paths
    [datt , pnm{n}]=uigetfile({'*.ims'}, 'Select Imaris files that you want to apply the function to (Include the currently opened file)','Multiselect', 'on');          
    if ischar(datt)
        datt = {datt};
    end
    fnm{n} = datt;
    cd(pnm{n})
    % Construct a question diolog with two options
    choice = questdlg(sprintf(['Are there more folders with files you want to look at?']), ...
    'Select Multiple Folders', 'Yes','No ', 'No ');    
end
n
%% Ask for the loading paths

%Input the Folder paths and File paths
pnmSave = uigetdir(pnm{1}, 'Select where you want to save your files to');          
pnmSave

%% Add in the batch loop structure
waitbar(0.2, vPD, 'Starting Loop');
for i = 1:size(pnm, 2)
   for j = 1:size(fnm{i}, 2)
       %% open the file
       pnm{i}
% % %        cd(pnm{i})
       waitbar(0.2, vPD, 'Opening File');
       vImarisApplication.FileOpen([pnm{i} fnm{i}{j}], '')
       pause(10)

        %% Read in the data and add a new channel to the matrix
% % %         vDataSet = vImarisApplication.GetDataSet.Clone;    %Re-write so matlab doesn't pull in the whole file
        waitbar(0.2, vPD, 'Pull Image Info');
        aSizeX = vImarisApplication.GetDataSet.GetSizeX;
        aSizeY = vImarisApplication.GetDataSet.GetSizeY;
        aSizeT = vImarisApplication.GetDataSet.GetSizeT;
        aSizeZ = vImarisApplication.GetDataSet.GetSizeZ;

        %% Evaluate the functions
        vAnswer = '';
        vName = '';
        for veqt = 1:neqts
            eval(sprintf(['vAnswer = vAnswer%i'], veqt));
            eval(sprintf(['vName = vName%i'], veqt));
            
            %Find the number of channels
            vLastC = vImarisApplication.GetDataSet.GetSizeC;
            waitbar(0.2, vPD, 'Create a new channel (This takes forever for no reason)');

            % Create a new channel directly in Imaris
            vImarisApplication.GetDataSet.SetSizeC(vLastC + 1);
            % Name the channel with the equation used
            vImarisApplication.GetDataSet.SetChannelName(vLastC, vName{1});

            %Evaluate the equation for each t slice
            for vTime = 1:aSizeT
              %Evaluate the equation for each z slice
              for vSlice = 1:aSizeZ
                for vChannel = 1:vLastC
                    if contains(vAnswer{1}, sprintf('ch%i', vChannel))
                        % works on double to allow division and prevent overflows
                        chdat = double(vImarisApplication.GetDataSet.GetDataSubVolumeAs1DArrayFloats(0, 0, vSlice-1, vChannel-1, vTime-1, aSizeX, aSizeY, 1));
                        % This sets the data from each channel = to the channel number          
                        eval(sprintf('ch%i = chdat;', vChannel));
                    end

                  % works on double to allow division and prevent overflows
                  % This sets the data from each channel = to the channel number
                end
                % Evaluates the given arithmatic/function 
                try
                  vData = eval(vAnswer{1});
                catch er
                  msgbox(sprintf(['Error while evaluating the expression.\n\n', ...
                    'Possible causes: invalid variable names (cH 1 instead of ch1 etc...), ', ...
                    'invalid operators (use .* instead of *)...\n\n', er.message]));
                  return;
                end  
                % Copys the data back to Imaris
                waitbar((vTime*vSlice)/(aSizeT*aSizeZ), vPD, ['Copying slice ' num2str(vTime) ', ' num2str(vSlice) ...
                        ' of (' num2str(aSizeT) ',' num2str(aSizeZ) ') back to Imaris']);
                try
                  if strcmp(vImarisApplication.GetDataSet.GetType,'eTypeUInt8')
                    vImarisApplication.GetDataSet.SetDataSubVolumeAs1DArrayBytes(uint8(vData), 0,0,vSlice-1,vLastC,vTime-1,aSizeX,aSizeY,1);
                  elseif strcmp(vImarisApplication.GetDataSet.GetType,'eTypeUInt16')
                    vImarisApplication.GetDataSet.SetDataSubVolumeAs1DArrayShorts(uint16(vData), 0,0,vSlice-1,vLastC,vTime-1,aSizeX,aSizeY,1);
                  elseif strcmp(vImarisApplication.GetDataSet.GetType,'eTypeFloat')
                    vImarisApplication.GetDataSet.SetDataSubVolumeAs1DArrayFloats(single(vData), 0,0,vSlice-1,vLastC,vTime-1,aSizeX,aSizeY,1);
                  end
                catch er
                  msgbox(sprintf(['The result of the expression is not a valid dataset.\n\n', ...
                    'Possible causes: invalid result size.\n\n', er.message]));
                  return
                end
              end
            end
        end
       
       %Save the file (im not sure if the pauses are helping or not)
       pause(20)
       waitbar(0.9, vPD, 'Saving File');
       vImarisApplication.FileSave([pnmSave '\Batched_' fnm{i}{j}], '');
       pause(60)
       clear vDataSet vData
       waitbar(1, vPD, 'Done!');
   end
end
close(vPD);


