function initialize_tests(fnm, functions)
    disp('inside of invoke tests')
    warning('off','all');
    addpath(['..' filesep 'Tests']);
    
    if ~exist('functions', 'var')
        functions = [];
    end
    
    disp("Test " + string(fnm) + " started.");
    if ~invoke_tests_helper(fnm, functions)
        disp('Test Failed on test');
        exit(1);
    else
        exit(0);
    end
end

function success = invoke_tests_helper(fnm, functions)
    disp('inside of invoke test helper')
    
    test_IO()
    
    success = true;
end